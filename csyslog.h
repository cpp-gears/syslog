#pragma once
#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <string>
#include <chrono>

class csyslog {
	static inline size_t levelVerbose{ 1 };
	static inline bool withTime{ false };
	void printTime() const;
public:
	inline void operator()(const char* fmt, ...) const { va_list args; va_start(args, fmt); if (withTime) printTime(); vfprintf(stdout, fmt, args); va_end(args); }
	inline void operator()(size_t lvl, const char* fmt, ...) const { if (levelVerbose >= lvl) { va_list args; va_start(args, fmt); if (withTime) printTime(); vfprintf(stdout, fmt, args); va_end(args); } }
	void bt(ssize_t deep = 20) const;
	inline std::string error(ssize_t nerr) const { return std::strerror(-(int)nerr); }
	std::string error(ssize_t nerr, const std::string& fmt) const;
	void open(size_t verbose, const std::string& filelog = {}, bool withTime = false) const;
	inline void flush() const { fflush(stdout); }
	inline bool is(size_t lvl) const { return levelVerbose >= lvl; }
};

static inline constexpr csyslog syslog;

#ifdef DEBUG
#define dbg(args...) {fprintf(stdout,"\033[1;90m[ %s:%ld ] ",__PRETTY_FUNCTION__,__LINE__); fprintf (stdout, args); fprintf (stdout, "\033[0m");}
#else
#define dbg(args...) ;
#endif // DEBUG
