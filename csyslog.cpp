#include "csyslog.h"
#include <system_error>
#include <execinfo.h>

std::string csyslog::error(ssize_t nerr, const std::string& fmt) const {
	auto serr{ std::strerror(-(int)nerr) };
	if (size_t size = std::snprintf(nullptr, 0, fmt.c_str(), serr); size) {
		auto buffer{ (char*)alloca(size + 1) };
		std::snprintf(buffer, size + 1, fmt.c_str(), serr);
		return { buffer };
	}
	return fmt;
}

void csyslog::bt(ssize_t deep) const {
	void* array[deep];
	fprintf(stderr, "\n");
	int size = backtrace(array, (int)deep);
	if (size) {
		char** messages = backtrace_symbols(array, size);
		for (int n = 0; n < size; n++) {
			fprintf(stderr, "[ TRACE ] #%2d %s\n", n, messages[n]);
		}
		free(messages);
	}
	else {
		fprintf(stderr, "[ TRACE ] No backtrace symbols\n");
	}
	fflush(stderr);
}

void csyslog::open(size_t verbose, const std::string& filelog, bool _withTime) const {
	if (!filelog.empty()) freopen(filelog.c_str(), "a+", stderr);
	levelVerbose = verbose;
	withTime = _withTime;
}

void csyslog::printTime() const {
	time_t rawTime;
	struct tm* timeinfo;
	char buffer[20];
	time(&rawTime);
	timeinfo = localtime(&rawTime);
	strftime(buffer, 20, "%y/%m/%d %H:%M:%S", timeinfo);
	printf("[ %s ]", buffer);
}
